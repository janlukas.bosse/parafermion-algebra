from typing import List, Union
from numbers import Number
from copy import deepcopy
from functools import singledispatch, singledispatchmethod, cmp_to_key
import sympy as smp


# ##############################################################################
# The ParaferionOperator Class
# ##############################################################################
class ParafermionOperator():
    def __init__(self, order: int, exponent: int, index):
        """Construct a parafermion operator.
        
        Arguments:
        ----------
        order : int
            The degree / order of the operator. op^order = 1
        exponent : int
            Exponent of the operator.
        index : ordered
            Site / index of the operator. Must come from a ordered set
        """
        self.order = order
        self.exponent = exponent
        self.index = index
        self.simplify()
    
    def simplify(self):
        "Get self into standard form."
        self.exponent %= self.order

    # Addition and Subtraction
    # ------------------------
    def __add__(self, other):
        return ParafermionProduct([self]) + other

    def __sub__(self, other):
        return ParafermionProduct([self]) - other


    # Multiplication    
    # --------------
    def __mul__(self, other):
        if isinstance(other, (ParafermionProduct, ParafermionSum)):
            return NotImplemented
        return ParafermionProduct([self]) * other
    
    def __rmul__(self, other):
        return self * other
        
    def __pow__(self, exponent):
        return ParafermionOperator(self.order, self.exponent * exponent,
                                   self.index)

    
    def __eq__(self, other):
        if self.order is not other.order:
            raise ValueError("Cannot compare ParafermionOperators of different order")
        return self.index == other.index and self.exponent == other.exponent 
        
    def __repr__(self):
        return f"γ_{self.index}^{self.exponent}"
    
    def _repr_latex_(self):
        return f"$\\gamma_{self.index}^{self.exponent}$"

    def sympify(self):
        return smp.var(f"gamma_{self.index}")**self.exponent
    

# ##############################################################################
# The ParafermionProduct Class
# ##############################################################################
class ParafermionProduct():
    def __init__(self,
                 factors: List[ParafermionOperator],
                 omega_exponent=0, prefactor=1, order=None):
        """Construct a product of parafermion operators.
        
        Arguments:
        ----------
        factors : List[ParafermionOperator]
            A list of parafermion operators that are the factors of the product.
        omega_exponent=1 : int
            The exponent of the preceeding omega.
        prefactor : Number
            A scalar prefactor for the product.
        """
        if not factors and order:
            self.prefactor = prefactor
            self.omega_exponent = omega_exponent
            self.order = order
            self.factors = []
            self.simplify()
            return

        if order:
            self.order = order
        else:
            self.order = factors[0].order
        if not all(self.order == op.order for op in factors):
            raise ValueError("All factors must be parafermion "
                             "operators of the same order.")
        
        self.factors = deepcopy(factors)
        self.prefactor = prefactor
        self.omega_exponent = omega_exponent
        self.simplify()
    
        
    def simplify(self):
        "Get self into standard form."
        didsomething = False
        i = 0
        removed_ops = 0
        while i < len(self)-1:
            op1 = self.factors[i-removed_ops]
            op2 = self.factors[i-removed_ops+1]
            if op1.index > op2.index:
                didsomething = True
                self.omega_exponent -= op1.exponent * op2.exponent
                self.factors[i], self.factors[i+1] = op2, op1
            if op1.index == op2.index:
                didsomething = True
                op1.exponent += op2.exponent
                self.factors.pop(i+1)
                removed_ops += 1
            i += 1
        
        if didsomething:
            self.simplify()
        self.omega_exponent %= self.order
        for op in self:
            op.simplify()

        # if we are working with sympy prefactors, simplify them
        try:
            self.prefactor.simplify()
        except:
            pass
            
        self.factors[:] = [op for op in self if op.exponent != 0]
    
    def hc(self):
        "get the hermitian conjugate operator"
        prefactor = self.prefactor.conjugate()
        omega_exponent = -self.omega_exponent
        order = self.order
        factors = deepcopy(self.factors)
        factors.reverse()
        for op in factors:
            op.exponent = order - op.exponent

        return ParafermionProduct(factors, omega_exponent, prefactor, order) 

    # Addition and subtraction
    # ------------------------
    @singledispatchmethod
    def __add__(self, other):
        if isinstance(other, ParafermionSum):
            return NotImplemented
        return self + ParafermionProduct([], order=self.order, prefactor=other)
    
    @__add__.register
    def _(self, other: ParafermionOperator):
        return self + ParafermionProduct([other])
    
    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        return self + (-1)*other
        
        
    # Multiplication
    # --------------
    @singledispatchmethod
    def __mul__(self, other):
        if isinstance(other, ParafermionSum):
            return NotImplemented
        return ParafermionProduct(self.factors,
                                  omega_exponent=self.omega_exponent,
                                  prefactor=self.prefactor * other,
                                  order=self.order)

    @__mul__.register
    def _(self, other: ParafermionOperator):
        return self * ParafermionProduct([other])


    @singledispatchmethod
    def __rmul__(self, other):
        return self * other
        
    @__rmul__.register
    def _(self, other: ParafermionOperator):
        return ParafermionProduct([other]) * self
    
    def __pow__(self, exponent):
        if exponent == 0:
            return ParafermionProduct([], order=self.order)
        out = deepcopy(self)
        for _ in range(exponent-1):
            out = out * self
        return out
    
    
    def __eq__(self, other):
        "Test equality of ParafermionProducts _up to_ the prefactor."
        return (self.factors == other.factors
                and self.omega_exponent == other.omega_exponent)
       
    def __repr__(self):
        out = f"{self.prefactor}*ω^{self.omega_exponent}"
        for op in self:
            out += f"*{op}"
        return out

    def __iter__(self):
        return iter(self.factors)
    
    def __len__(self):
        return len(self.factors)

    def _repr_latex_(self):
        try:
            out = f"${self.prefactor._repr_latex_()[1:-1]}\\omega^{self.omega_exponent}"
        except AttributeError:
            out = f"${self.prefactor}\\omega^{self.omega_exponent}"
        for op in self:
            out += op._repr_latex_()[1:-1]
            
        out += "$"
        return out

    def sympify(self):
        out = smp.sympify(self.prefactor)
        out *= smp.var("omega")**self.omega_exponent
        for term in self:
            out *= term.sympify()
        return out
    

@ParafermionProduct.__add__.register
def _(self, other: ParafermionProduct):
    if self.order is not other.order:
        raise ValueError("Cannot add Parafermion operators of different orders")
    return ParafermionSum([self, other])
    
@ParafermionProduct.__mul__.register
def _(self, other: ParafermionProduct):
    if self.order is not other.order:
        raise ValueError("Cannot multiply Parafermion operators of different orders")
    prefactor = self.prefactor * other.prefactor
    omega_exponent = self.omega_exponent + other.omega_exponent
    factors = self.factors + other.factors
    return ParafermionProduct(factors, omega_exponent, prefactor, order=self.order)


def omega(order, exponent=1, prefactor=1):
    return ParafermionProduct([], omega_exponent=exponent, order=order)


def productcompare(prod1, prod2):
    for op1, op2 in zip(prod1.factors, prod2.factors):
        if op1.index > op2.index:
            return 1
        elif op1.index < op2.index:
            return -1
        elif op1.exponent > op2.exponent:
            return 1
        elif op1.exponent < op2.exponent:
            return -1
    if len(prod1) > len(prod2):
        return 1
    elif len(prod1) < len(prod2):
        return -1
    return prod1.omega_exponent - prod2.omega_exponent

# ##############################################################################
# The ParaferionSum Class
# ##############################################################################
class ParafermionSum():
    def __init__(self, terms: List[ParafermionProduct], order=None):
        """Create a sum of ParafermionProducts
        
        Arguments:
        ----------
        terms : List[ParafermionProduct]
            A list of terms in the sum
        """
        if not terms:
            self.terms = []
            if not order:
                raise ValueError("need to pass an order if the term list is empty")
            self.order = order
            return

        self.order = terms[0].order
        if not all(self.order == op.order for op in terms):
            raise ValueError("All terms must be parafermion "
                             "products of the same order.")
        self.terms = deepcopy(terms)
        self.simplify()
        
        
    def simplify(self):
        for term in self:
            term.simplify()
        
        i = 0
        notdonei = True
        while notdonei:
            termi = self.terms[i]
            if i+1 < len(self):
                j = i+1
                notdonej = True
            else:
                notdonej = False
            while notdonej:
                termj = self.terms[j]
                if termi == termj:
                    termi.prefactor += termj.prefactor
                    self.terms.pop(j)
                    j -= 1
                j += 1
                if j == len(self):
                    notdonej = False
            i += 1
            if i+1 >= len(self):
                notdonei = False
        
        self.terms[:] = [term for term in self if term.prefactor != 0]
        self.terms.sort(key=cmp_to_key(productcompare))
        

    def hc(self):
        "The hermitian conjugate operator"
        terms = [term.hc() for term in self.terms]
        return ParafermionSum(terms)

    def get_prefactor(self, term):
        "Return the prefactor of the term. Returns 0 if term is not in the sum"
        assert(self.order == term.order)
        for t in self.terms:
            if t == term:
                return t.prefactor 
        return 0

    def get_total_prefactor(self, term, sympy=False):
        """Return the sum of all prefactors of terms with the same operators.
        The difference to `get_prefactor` is that it doesn't care for the power
        of omega. Returns a sympy expression instead of a ParafermionSum
        if sympy=True.
        """
        assert(self.order == term.order)

        w = smp.var("omega") if sympy else omega(self.order) 
        prefactor = 0
        term.omega_exponent = 0
        for i in range(term.order):
            prefactor += w**i * self.get_prefactor(term)
            term.omega_exponent += 1
            term.omega_exponent %= term.order
        return prefactor 

        
    # Addition and subtraction
    # ------------------------
    @singledispatchmethod
    def __add__(self, other):
        return self + ParafermionProduct([], order=self.order, prefactor=other)
        
    @__add__.register
    def _(self, other: ParafermionProduct):
        assert other.order == self.order
        return self + ParafermionSum([other], order=self.order)
        
    @__add__.register
    def _(self, other: ParafermionOperator):
        assert other.order == self.order
        return self + ParafermionProduct([other], order=self.order)

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        return self + (-1) * other
    
    
    # Multiplication
    # --------------
    @singledispatchmethod
    def __mul__(self, other):
        out = deepcopy(self)
        for term in out.terms:
            term.prefactor *= other
        return out
    
    @__mul__.register
    def _(self, other: ParafermionProduct):
        return self * ParafermionSum([other])
    
    @__mul__.register
    def _(self, other: ParafermionOperator):
        return self * ParafermionProduct([other])
    
    
    @singledispatchmethod
    def __rmul__(self, other):
        return self * other
        
    @__rmul__.register
    def _(self, other: ParafermionProduct):
        return ParafermionSum([other]) * self
    
    @__rmul__.register
    def _(self, other: ParafermionOperator):
        return ParafermionProduct([other]) * self
    

    def __eq__(self, other):
        return (self - other).terms == []
        

    def __len__(self):
        return len(self.terms)
    
    def __iter__(self):
        return iter(self.terms)
        

    def __repr__(self):
        if not self:
            return "0"

        out = f"{self.terms[0]}"
        for term in self.terms[1:]:
            out += f" + {term}"
        return out
    
    def _repr_latex_(self):
        if not self:
            return "$0$"

        out = self.terms[0]._repr_latex_()[:-1]
        for term in self.terms[1:]:
            out += f" + {term._repr_latex_()[1:-1]}"
        out += "$"
        return out

    def sympify(self):
        return sum(term.sympify() for term in self)
    


@ParafermionSum.__add__.register
def _(self, other: ParafermionSum):
    if self.order is not other.order:
        raise ValueError("Cannot add Parafermion operators of different orders")
    terms = self.terms + other.terms
    return ParafermionSum(terms, order=self.order)

@ParafermionSum.__mul__.register
def _(self, other: ParafermionSum):
    if self.order is not other.order:
        raise ValueError("Cannot multiply Parafermion operators of different orders")
    terms = [term1 * term2 for term1 in self.terms for term2 in other.terms]
    return ParafermionSum(terms)
