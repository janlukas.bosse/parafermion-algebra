import sys
sys.path.append('..')

from ParafermionAlgebra import ParafermionOperator, ParafermionProduct

gamma1 = ParafermionOperator(3, 1, 1)
gamma2 = ParafermionOperator(3, 1, 2)
gamma3 = ParafermionOperator(3, 2, 1)

def test_inequality():
    assert gamma1 != gamma2

def test_multiplication():
    assert gamma1 * gamma1 == ParafermionProduct([gamma3])

def test_scalar_multiplication():
    assert 5 * gamma1 == ParafermionProduct([gamma1], prefactor=5)
