import sys
sys.path.append('..')

from ParafermionAlgebra import ParafermionOperator, ParafermionProduct
import sympy as smp

gamma1 = ParafermionOperator(3, 1, 1)
gamma2 = ParafermionOperator(3, 1, 2)
gamma3 = ParafermionOperator(3, 2, 1)

prod1 = ParafermionProduct([gamma1, gamma2, gamma3], 1, smp.Rational(1,1))
prod2 = ParafermionProduct([gamma2, gamma3], 0, smp.Rational(-1,1))
prod3 = ParafermionProduct([gamma1, gamma2], 0, smp.sin(3))

def test_multiply():
    a = prod3 * prod2
    b = prod1 * gamma2
    assert a == b
